inkjs = require("inkjs")

saveChoice = (index) ->
  window.progress.push(index)
  localStorage.setItem("progress", JSON.stringify(window.progress))

displayText = (s, interactive = true) ->
  while (s.canContinue)
    paragraphs = s.Continue().split("\n")
    if interactive
      delay = 1000
    for i in paragraphs
      if i != ""
        html = $.parseHTML(i)
        block = $('<p>').html(html)
        if interactive
          block.hide()
        $("#content").append(block)
        if interactive
          block.fadeIn(delay)
          delay += 500

continueToNextChoice = (s) ->
  displayText(s, true)
  scrollTo = $('#options').offset().top
  if (s.currentChoices.length > 0)
    $("#options").html("").hide()
    for choice in s.currentChoices
      $("#options").append("<li><a href='#' id='choice-#{choice.index}' data-index=#{choice.index}>#{choice.text}</a></li>")
    $("#options").fadeIn(800)
    $("#options li a").click(() ->
      s.ChooseChoiceIndex($(this).data("index"))
      saveChoice($(this).data("index"))
      continueToNextChoice(s)
      return false
    )
  else
    $("#content").append("<p>THE END</p>")
    $("#options").html("")
  $('html, body').animate({
    scrollTop: scrollTo
  }, 800)

loadGame = (s) ->
  for index in window.progress
    displayText(s, false)
    s.ChooseChoiceIndex(index)

clearProgress = () ->
  window.progress = []

$.ajax({
  url: 'fogg.ink.json',
  dataType: 'text',
  success: (data) ->
    progress = localStorage.getItem("progress")
    if progress?
      window.progress = JSON.parse(progress)
    else
      window.progress = []
    s = new inkjs.Story(data)
    if window.progress != []
      loadGame(s)
    continueToNextChoice(s)
})
